USE ram_slam_2015;


LOAD DATA INFILE '/var/lib/mysql-files/2015_south_ram_slam_t20.match_details.csv' INTO TABLE match_details 
 FIELDS TERMINATED BY ','   LINES TERMINATED BY '\n' IGNORE 1 LINES;

LOAD DATA INFILE '/var/lib/mysql-files/2015_south_ram_slam_t20.first_innings_batting.csv' INTO TABLE  first_innings_batting FIELDS TERMINATED BY ','   LINES TERMINATED BY '\n' IGNORE 1 LINES;

LOAD DATA INFILE '/var/lib/mysql-files/2015_south_ram_slam_t20.second_innings_batting.csv' INTO TABLE  second_innings_batting FIELDS TERMINATED BY ','   LINES TERMINATED BY '\n' IGNORE 1 LINES;

LOAD DATA INFILE '/var/lib/mysql-files/2015_south_ram_slam_t20.second_innings_bowling.csv' INTO TABLE  second_innings_bowling FIELDS TERMINATED BY ','   LINES TERMINATED BY '\n' IGNORE 1 LINES;

LOAD DATA INFILE '/var/lib/mysql-files/2015_south_ram_slam_t20.first_innings_bowling.csv' INTO TABLE  first_innings_bowling FIELDS TERMINATED BY ','   LINES TERMINATED BY '\n' IGNORE 1 LINES;

LOAD DATA INFILE '/var/lib/mysql-files/2015_south_ram_slam_t20.score_details.csv' INTO TABLE  score_details FIELDS TERMINATED BY ','   LINES TERMINATED BY '\n' IGNORE 1 LINES;

